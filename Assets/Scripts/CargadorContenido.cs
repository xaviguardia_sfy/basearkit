﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CargadorContenido : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
		#if !UNITY_EDITOR
        	StartCoroutine(CargarEscenaContenido());
		#endif


    }

    IEnumerator CargarEscenaContenido()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Prolia/ProliaContent", LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }


    // Update is called once per frame
    void Update()
    {

    }
}
