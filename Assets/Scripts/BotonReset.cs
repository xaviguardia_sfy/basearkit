﻿#if UNITY_WSA
    using HoloToolkit.Unity.InputModule;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_WSA
    public class BotonReset : MonoBehaviour, IInputClickHandler, IInputHandler, IFocusable
#endif
#if !UNITY_WSA
    public class BotonReset : MonoBehaviour
#endif
{
    FichaManager fichaManager;
    Animator controlAnimacion;
    float lastEvent;
    public void OnFocusEnter()
    {
        Debug.Log("Focus Enter");
        controlAnimacion.Play("blinkreset");
    }

    public void OnFocusExit()
    {
        Debug.Log("Focus exit");
        controlAnimacion.Play("Idle");
    }

#if UNITY_WSA
    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("Click on reset");
        fichaManager.StartResetScene();
    }

    public void OnInputDown(InputEventData eventData)
    {
    }

    public void OnInputUp(InputEventData eventData)
    {
    }


    // Use this for initialization
    void Start () {
        GameObject go = GameObject.Find("ControlEscenas");
        if (go == null)
        {
            Debug.LogError("No encuentro el GameObject ControlEscenas");
        }
        Debug.Log("Contenedor Fichas Encontrado, desde boton");
        fichaManager = go.GetComponent<FichaManager>();
        controlAnimacion = gameObject.GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        controlAnimacion.StopPlayback();
        Debug.Log("clicked on me with mouse");
        fichaManager.StartResetScene();
    }

    void OnMouseEnter()
    {
        Debug.Log("mOUSE eNTER");
        controlAnimacion.Play("blinkreset");
    }

    void OnMouseExit()
    {
        controlAnimacion.Play("Idle");
    }
    // Update is called once per frame
    void Update () {
		
	}
	#endif
	#if UNITY_IOS
		void Start() {
			var go = GameObject.Find ("BotonReset");
			go.SetActive (false);
		}
	#endif
}
