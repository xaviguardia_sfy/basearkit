﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeMesh : MonoBehaviour {

	public float DuracionFade = .5f;
	public FadeType fadeType = FadeType.FadeIn;
	private bool hasSkinnedMesh = false;
	// Use this for initialization
	void Start () {
		if (GetComponent<SkinnedMeshRenderer> () != null) {
			hasSkinnedMesh = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable()
	{
		if (!hasSkinnedMesh) {
			if (fadeType == FadeType.FadeOut) {
				StartCoroutine (Extensions.FadeOut3D<Renderer> (transform, 0, true, DuracionFade));
				Debug.Log (name);
				Debug.Log ("fadin in");
			}
			if (fadeType == FadeType.FadeIn) {
				StartCoroutine (Extensions.FadeIn3D<Renderer> (transform, 1, true, DuracionFade));
				Debug.Log (name);
				Debug.Log ("fadin out");
			}
		} else {
			if (fadeType == FadeType.FadeOut) {
				StartCoroutine (Extensions.FadeOut3D<SkinnedMeshRenderer> (transform, 0, true, DuracionFade));
			}
			if (fadeType == FadeType.FadeIn) {
				StartCoroutine (Extensions.FadeIn3D<SkinnedMeshRenderer> (transform, 1, true, DuracionFade));
			}
		}
	}
}

public enum FadeType {
	FadeIn,
	FadeOut
}

public static partial  class Extensions {
	public static IEnumerator FadeOut3D<T> ( Transform t, float targetAlpha, bool isVanish, float duration) 
		where T:  Renderer
	{
		var sr = (Renderer)t.GetComponent<T> ();
		float diffAlpha = (targetAlpha - sr.material.color.a);

		float counter = 0;
		while (counter < duration) {
			float alphaAmount = sr.material.color.a + (Time.deltaTime * diffAlpha) / duration;
			sr.material.color = new Color (sr.material.color.r, sr.material.color.g, sr.material.color.b, alphaAmount);

			counter += Time.deltaTime;
			yield return null;
		}
		sr.material.color = new Color (sr.material.color.r, sr.material.color.g, sr.material.color.b, targetAlpha);
		if (isVanish) {
			sr.transform.gameObject.SetActive (false);
		}
	}

	public static IEnumerator FadeIn3D<T> ( Transform t, float targetAlpha, bool isVanish, float duration) 
		where T:  Renderer
	{
		var sr = (Renderer)t.GetComponent<T> ();
		Debug.Log (sr.material.color.a);
		sr.material.color = new Color (sr.material.color.r, sr.material.color.g, sr.material.color.b, .3f);

		float diffAlpha = (targetAlpha - sr.material.color.a);

		float counter = 0;
		while (counter < duration) {
			float alphaAmount = sr.material.color.a + (Time.deltaTime * diffAlpha) / duration;
			sr.material.color = new Color (sr.material.color.r, sr.material.color.g, sr.material.color.b, alphaAmount);

			counter += Time.deltaTime;
			yield return null;
		}
		sr.material.color = new Color (sr.material.color.r, sr.material.color.g, sr.material.color.b, targetAlpha);

	}

}