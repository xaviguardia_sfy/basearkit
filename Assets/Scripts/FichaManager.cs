﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FichaManager : MonoBehaviour
{

    public List<GameObject> Escenas;
    public GameObject EscenaActiva;
    public GameObject WorldContent;
    public GameObject fakeCube;
    public TextMesh counterText;
    bool TimerActive = false;
    public GameObject HololensCamera;
    private float ResetTimeLeft;

    // Use this for initialization
    void Start()
    {
#if UNITY_WSA
            HololensCamera = GameObject.Find("HoloLensCamera");
            WorldContent = GameObject.Find("WorldContent");
            GameObject goCounter = GameObject.Find("counter");
            counterText = goCounter.GetComponent<TextMesh>();
            fakeCube = GameObject.Find("fakeCube");
            fakeCube.transform.SetParent(HololensCamera.transform);
            fakeCube.SetActive(false);
            GameObject ipadOnly = GameObject.Find("iPadOnly");
            if(ipadOnly != null) { 
               ipadOnly.SetActive(false);
            }
#endif
#if UNITY_IOS
              GameObject hololensOnly = GameObject.Find("HololensOnly");
			  if(hololensOnly != null) {
                 hololensOnly.SetActive(false);
			   }
			// button handler for UI next button on iPad
			Button  nextButton = GameObject.Find("NextButton").GetComponent<Button>();
			nextButton.onClick.AddListener(Next);
			Button  homeButton = GameObject.Find("HomeButton").GetComponent<Button>();
			homeButton.onClick.AddListener(Home);
#endif
        foreach (GameObject ficha in Escenas){
            ficha.SetActive(false);
        }
        EscenaActiva.SetActive(true);
		FixUIButton ();
		
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_WSA
                if (TimerActive == true)
                {
                    ResetTimeLeft = ResetTimeLeft - Time.deltaTime;
                    counterText.text = Math.Round(ResetTimeLeft,1).ToString();
                    if (ResetTimeLeft <= 0)
                    {
                        EndResetScene();
                    }
                } 
#endif
    }

	/// <summary>
	/// on iPad use the same text as the button
	/// </summary>
	private void FixUIButton() {
		#if UNITY_IOS 
			// localizar boton de siguiente Escena
			ButtonClick Boton =EscenaActiva.GetComponentInChildren<ButtonClick>();
			GameObject parent = Boton.gameObject;
			TextMesh texto =parent.GetComponentInChildren<TextMesh>();
			Text goText = GameObject.Find("NextButtonText").GetComponent<Text>();
			goText.text=texto.text;
			Debug.Log("El texto del mesh es:"+texto.text);
		#endif
	}
    public void Next()
    {
        // determino la siguiente
        GameObject siguienteEscena = Escenas.NextOf(EscenaActiva);
        Debug.Log("Siguiente Escena:");
        Debug.Log(siguienteEscena);
        if (siguienteEscena == null)
        {
            Debug.Log("No hay siguiente ficha, ponemos la primera");
            siguienteEscena = Escenas.FirstOrDefault();
        }
        if (siguienteEscena != null)
        {
            EscenaActiva.SetActive(false);
            siguienteEscena.SetActive(true);
            EscenaActiva = siguienteEscena;
			FixUIButton ();
        }
    }

	public void Home() {
		GameObject primeraEscena = Escenas.First ();
		foreach (GameObject ficha in Escenas){
			ficha.SetActive(false);
		}
		EscenaActiva = primeraEscena;
		EscenaActiva.SetActive (true);
	}
#if UNITY_WSA
        public void StartResetScene() {
            WorldContent.SetActive(false);
            ResetTimeLeft = 10f;
            TimerActive = true;
            fakeCube.SetActive(true);
            Debug.Log("Activo Timer");
        }

        public void EndResetScene() {
            Debug.Log("End Reset Scene");
            WorldContent.SetActive(true);
            TimerActive = false;
            WorldContent.transform.position = fakeCube.transform.position;
            var fwd = Camera.main.transform.forward;
            fwd.y = 0.0f;
            WorldContent.transform.rotation = Quaternion.LookRotation(fwd);
            // WorldContent.transform.rotation;
            fakeCube.SetActive(false);
        }
#endif

}

public  static partial class Extensions
{
    public static T NextOf<T>(this IList<T> list, T item)
    {
        var indexOf = list.IndexOf(item);
        return list[indexOf == list.Count - 1 ? 0 : indexOf + 1];
    }
}
