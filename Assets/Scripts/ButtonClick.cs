﻿#if UNITY_WSA
    using HoloToolkit.Unity.InputModule;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


#if UNITY_WSA
    public class ButtonClick : MonoBehaviour, IInputClickHandler, IInputHandler, IFocusable
#endif
#if !UNITY_WSA
    public class ButtonClick : MonoBehaviour
#endif
{
    FichaManager fichaManager;
    public string fichaName;

    // Use this for initialization
    void Start()
    {
        GameObject go = GameObject.Find("ControlEscenas");
        if (go == null)
        {
            Debug.LogError("No encuentro el GameObject ControlEscenas");
        }
        Debug.Log("Contenedor Fichas Encontrado, desde boton");
        fichaManager = go.GetComponent<FichaManager>();

        fichaName = gameObject.transform.parent.gameObject.name;

        // escena1 = GameObject.Find("Ficha1");
        // espinaDorsal = GameObject.Find("espinadorsal");
        // animatorEspinaDorsal= espinaDorsal.GetComponent<Animator>();
        // espinaDorsal.SetActive(false);
    }

    public void OnFocusEnter()
    {
        Debug.Log("Got focus");
    }

    public void OnFocusExit()
    {
        Debug.Log("Lost focus");
    }

    private void CambioEscena()
    {
        fichaManager.Next();
        //  escena1.SetActive(false);
        //  espinaDorsal.SetActive(true);
        //  animatorEspinaDorsal.Play("gira");
    }
    void OnMouseDown()
    {
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			Debug.Log("Touched the UI");
			return;
		}
		if (EventSystem.current.IsPointerOverGameObject() ||
			EventSystem.current.currentSelectedGameObject != null) {
			Debug.Log ("Touched the UI");
			return;
		}

        CambioEscena();
        Debug.Log("clicked on me with mouse");
    }

#if UNITY_WSA
    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("cLICKED ON ME WITH holohand");
         CambioEscena();
    }

    public void OnInputDown(InputEventData eventData)
    {
       // CambioEscena();
        Debug.Log("down on me");
    }

    public void OnInputUp(InputEventData eventData)
    {
        Debug.Log("Up on me");
    }
#endif


    // Update is called once per frame
    void Update()
    {

    }
}
