﻿
#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class GenerateImageAnchor : MonoBehaviour {


	[SerializeField]
	private ARReferenceImage referenceImage;

	[SerializeField]
	private GameObject prefabToGenerate;


	private GameObject _WorldObjectToCenter;
	
	private GameObject imageAnchorGO;
	

	// Use this for initialization
	void Start () {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;

	}

	void AddImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor added");
		if (arImageAnchor.referenceImageName == referenceImage.imageName) {
			Vector3 position = UnityARMatrixOps.GetPosition (arImageAnchor.transform);
			Quaternion rotation = UnityARMatrixOps.GetRotation (arImageAnchor.transform);
			WorldObjectToCenter().transform.position = position;
			WorldObjectToCenter().transform.rotation = rotation;
			WorldObjectToCenter().transform.Rotate(Vector3.right * 90);
			// xgl es duplicava
			if (imageAnchorGO == null) {
		
				imageAnchorGO = Instantiate<GameObject> (prefabToGenerate, position, rotation);
			} else {
				Debug.Log("Already exists anchor, we just update it");
				UpdateImageAnchor (arImageAnchor);
			}
		}
	}

	void UpdateImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor updated");
		if (arImageAnchor.referenceImageName == referenceImage.imageName) {
			imageAnchorGO.transform.position = UnityARMatrixOps.GetPosition (arImageAnchor.transform);
			imageAnchorGO.transform.rotation = UnityARMatrixOps.GetRotation (arImageAnchor.transform);
			WorldObjectToCenter().transform.position = UnityARMatrixOps.GetPosition (arImageAnchor.transform);
			WorldObjectToCenter().transform.rotation = UnityARMatrixOps.GetRotation (arImageAnchor.transform);
			WorldObjectToCenter().transform.Rotate(Vector3.right * 90);
		}

	}

	private GameObject WorldObjectToCenter() {
		if (_WorldObjectToCenter == null) {
			_WorldObjectToCenter = GameObject.Find ("WorldContent");
		}
		return _WorldObjectToCenter;
	}

	void RemoveImageAnchor(ARImageAnchor arImageAnchor)
	{
		Debug.Log ("image anchor removed");
		if (imageAnchorGO) {
			GameObject.Destroy (imageAnchorGO);
		}

	}

	void OnDestroy()
	{
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
	}

	public void SuspendTracking() {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;
		if (imageAnchorGO != null) {
			imageAnchorGO.SetActive(false);
		}

	}

	public void ResumeTracking() {
		UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
		UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
		if (imageAnchorGO != null) {
			imageAnchorGO.SetActive(true);
		}

	}
	// Update is called once per frame
	void Update () {
		
	}
}
#endif