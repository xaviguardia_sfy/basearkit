﻿#if UNITY_IOS

using UnityEngine;
using UnityEngine.Networking.PlayerConnection;
using System.Text;
using UnityEngine.XR.iOS.Utils;

#if UNITY_EDITOR

using UnityEditor.Networking.PlayerConnection;

namespace UnityEngine.XR.iOS
{
	public class ARKitRemoteConnection : MonoBehaviour
	{
        [Header("AR Config Options")]
		public UnityARAlignment startAlignment = UnityARAlignment.UnityARAlignmentGravity;
		public UnityARPlaneDetection planeDetection = UnityARPlaneDetection.Horizontal;
		public bool getPointCloud = true;
		public bool enableLightEstimation = true;
		public bool enableAutoFocus = true;

		[Header("Run Options")]
		public bool resetTracking = true;
		public bool removeExistingAnchors = true;

		EditorConnection editorConnection ;

		int currentPlayerID = -1;
		string guimessage = "none";

		Texture2D remoteScreenYTex;
		Texture2D remoteScreenUVTex;

		bool bTexturesInitialized;

		// Use this for initialization
		void Start () {
#if UNITY_IOS
            bTexturesInitialized = false;


			editorConnection = EditorConnection.instance;
			editorConnection.Initialize ();
			editorConnection.RegisterConnection (PlayerConnected);
			editorConnection.RegisterDisconnection (PlayerDisconnected);
			editorConnection.Register (ConnectionMessageIds.updateCameraFrameMsgId, UpdateCameraFrame);
			editorConnection.Register (ConnectionMessageIds.addPlaneAnchorMsgeId, AddPlaneAnchor);
			editorConnection.Register (ConnectionMessageIds.updatePlaneAnchorMsgeId, UpdatePlaneAnchor);
			editorConnection.Register (ConnectionMessageIds.removePlaneAnchorMsgeId, RemovePlaneAnchor);
			editorConnection.Register (ConnectionMessageIds.screenCaptureYMsgId, ReceiveRemoteScreenYTex);
			editorConnection.Register (ConnectionMessageIds.screenCaptureUVMsgId, ReceiveRemoteScreenUVTex);
#endif
		}

		void PlayerConnected(int playerID)
		{
			currentPlayerID = playerID;

		}

		void OnGUI()
		{
#if UNITY_IOS
            if (!bTexturesInitialized) 
			{
				if (currentPlayerID != -1) {
					guimessage = "Connected to ARKit Remote device : " + currentPlayerID.ToString ();

					if (GUI.Button (new Rect ((Screen.width / 2) - 200, (Screen.height / 2) - 200, 400, 100), "Start Remote ARKit Session")) 
					{
						SendInitToPlayer ();
					}
				} 
				else 
				{
					guimessage = "Please connect to player in the console menu";
				}

				GUI.Box (new Rect ((Screen.width / 2) - 200, (Screen.height / 2) + 100, 400, 50), guimessage);
			}
#endif
		}

		void PlayerDisconnected(int playerID)
		{
			if (currentPlayerID == playerID) {
				currentPlayerID = -1;
			}
		}

		void OnDestroy()
		{
#if UNITY_2017_1_OR_NEWER
			if(editorConnection != null) {
				editorConnection.DisconnectAll ();
			}
#endif
		}


		void InitializeTextures(UnityARCamera camera)
		{
			int yWidth = camera.videoParams.yWidth;
			int yHeight = camera.videoParams.yHeight;
			int uvWidth = yWidth / 2;
			int uvHeight = yHeight / 2;
			if (remoteScreenYTex == null || remoteScreenYTex.width != yWidth || remoteScreenYTex.height != yHeight) {
				if (remoteScreenYTex) {
					Destroy (remoteScreenYTex);
				}
				remoteScreenYTex = new Texture2D (yWidth, yHeight, TextureFormat.R8, false, true);
			}
			if (remoteScreenUVTex == null || remoteScreenUVTex.width != uvWidth || remoteScreenUVTex.height != uvHeight) {
				if (remoteScreenUVTex) {
					Destroy (remoteScreenUVTex);
				}
				remoteScreenUVTex = new Texture2D (uvWidth, uvHeight, TextureFormat.RG16, false, true);
			}

			bTexturesInitialized = true;
		}

		void UpdateCameraFrame(MessageEventArgs mea)
		{
#if UNITY_IOS
            serializableUnityARCamera serCamera = mea.data.Deserialize<serializableUnityARCamera> ();

			UnityARCamera scamera = new UnityARCamera ();
			scamera = serCamera;

			InitializeTextures (scamera);

			UnityARSessionNativeInterface.SetStaticCamera (scamera);
			UnityARSessionNativeInterface.RunFrameUpdateCallbacks ();
#endif
		}

		void AddPlaneAnchor(MessageEventArgs mea)
		{
#if UNITY_IOS
            serializableUnityARPlaneAnchor serPlaneAnchor = mea.data.Deserialize<serializableUnityARPlaneAnchor> ();

			ARPlaneAnchor arPlaneAnchor = serPlaneAnchor;
			UnityARSessionNativeInterface.RunAddAnchorCallbacks (arPlaneAnchor);
#endif
		}

		void UpdatePlaneAnchor(MessageEventArgs mea)
		{
#if UNITY_EDITOR && UNITY_IOS
            serializableUnityARPlaneAnchor serPlaneAnchor = mea.data.Deserialize<serializableUnityARPlaneAnchor> ();

			ARPlaneAnchor arPlaneAnchor = serPlaneAnchor;
			UnityARSessionNativeInterface.RunUpdateAnchorCallbacks (arPlaneAnchor);
#endif
		}

		void RemovePlaneAnchor(MessageEventArgs mea)
		{
#if UNITY_IOS
            serializableUnityARPlaneAnchor serPlaneAnchor = mea.data.Deserialize<serializableUnityARPlaneAnchor> ();

			ARPlaneAnchor arPlaneAnchor = serPlaneAnchor;
			UnityARSessionNativeInterface.RunRemoveAnchorCallbacks (arPlaneAnchor);
#endif
		}

		void ReceiveRemoteScreenYTex(MessageEventArgs mea)
		{
			if (!bTexturesInitialized)
				return;
			remoteScreenYTex.LoadRawTextureData(mea.data);
			remoteScreenYTex.Apply ();
			UnityARVideo arVideo = Camera.main.GetComponent<UnityARVideo>();
			if (arVideo) {
				arVideo.SetYTexure(remoteScreenYTex);
			}

		}

		void ReceiveRemoteScreenUVTex(MessageEventArgs mea)
		{
			if (!bTexturesInitialized)
				return;
			remoteScreenUVTex.LoadRawTextureData(mea.data);
			remoteScreenUVTex.Apply ();
			UnityARVideo arVideo = Camera.main.GetComponent<UnityARVideo>();
			if (arVideo) {
				arVideo.SetUVTexure(remoteScreenUVTex);
			}

		}


		void SendInitToPlayer()
		{
			serializableFromEditorMessage sfem = new serializableFromEditorMessage ();
			sfem.subMessageId = SubMessageIds.editorInitARKit;
			serializableARSessionConfiguration ssc = new serializableARSessionConfiguration (startAlignment, planeDetection, getPointCloud, enableLightEstimation, enableAutoFocus); 
			UnityARSessionRunOption roTracking = resetTracking ? UnityARSessionRunOption.ARSessionRunOptionResetTracking : 0;
			UnityARSessionRunOption roAnchors = removeExistingAnchors ? UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors : 0;
			sfem.arkitConfigMsg = new serializableARKitInit (ssc, roTracking | roAnchors);
			SendToPlayer (ConnectionMessageIds.fromEditorARKitSessionMsgId, sfem);
		}

		void SendToPlayer(System.Guid msgId, byte[] data)
		{
			editorConnection.Send (msgId, data);
		}

		public void SendToPlayer(System.Guid msgId, object serializableObject)
		{
#if UNITY_IOS
            byte[] arrayToSend = serializableObject.SerializeToByteArray ();
			SendToPlayer (msgId, arrayToSend);
#endif
		}


		// Update is called once per frame
		void Update () {
			
		}

	}
}
#endif
#endif