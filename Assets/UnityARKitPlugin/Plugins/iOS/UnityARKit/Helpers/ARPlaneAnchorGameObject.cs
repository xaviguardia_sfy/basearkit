﻿#if UNITY_IOS
using System;

namespace UnityEngine.XR.iOS
{
	public class ARPlaneAnchorGameObject
	{
		public GameObject gameObject;
		public ARPlaneAnchor planeAnchor;
	}
}

#endif